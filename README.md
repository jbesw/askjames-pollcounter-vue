# askJames-poll-counter

A (very) simple Vue application to enable voting across large numbers of users. The voting buttons trigger an API Gateway call to a Lambda function which stores the vote in DynamoDB. The front-end refreshes every 5 seconds.

This code is provided without any warranty. You are responsible for any AWS charges you incur. This is only provided for educational purposes.

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```
# Questions

Please contact James Beswick @jbesw on Twitter. Feel free to use this code however you choose, though no warranty is implied.